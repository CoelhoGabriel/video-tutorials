import mongoose from 'mongoose';

describe('TestMongoDB', () => {
  test('Should execute operations on mongodb', async () => {
    const connection = await mongoose
      .createConnection()
      .openUri('mongodb://0.0.0.0:27017/event-store');
    const usersCollection = connection.collection('usuarios');
    await usersCollection.insertOne({
      nome: 'Luã',
      sobrenome: 'Faria',
    });
    console.log(usersCollection);
    let insertedUser = await usersCollection.findOne({ nome: 'Luã' });
    expect(insertedUser).toBeDefined();
    expect(insertedUser?.nome).toEqual('Luã');
    expect(insertedUser?.sobrenome).toEqual('Faria');

    await usersCollection.deleteOne({ nome: 'Luã' });
    insertedUser = await usersCollection.findOne({ nome: 'Luã' });
    expect(insertedUser).toBeNull();

    await mongoose.disconnect();
  });
});
