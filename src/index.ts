import createExpressApp from '@/app/express';
import { createConfig } from '@/config';
import env from '@/env';

export async function start(): Promise<{ app: any; config: any }> {
  const config = await createConfig({ env });
  const app = createExpressApp({ config, env });
  app.listen(env.port, signalAppStart);

  return {
    app,
    config,
  };
}

function signalAppStart(): void {
  console.log(`${env.appName} started`);
  console.table([
    ['Port', env.port],
    ['Environment', env.env],
  ]);
}
