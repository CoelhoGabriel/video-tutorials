// import packageJson from '../package.json';
import dotenv from 'dotenv';

const envResult = dotenv.config();

if (envResult.error) {
  console.error(`[ERROR] env failed to load: ${envResult.error}`);

  process.exit(1);
}

function requireFromEnv(key): string {
  if (!process.env[key]) {
    console.error(`[APP ERROR] Missing env variable: ${key}`);

    return process.exit(1);
  }

  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  return process.env[key]!;
}

export default {
  appName: requireFromEnv('APP_NAME'),
  env: requireFromEnv('NODE_ENV'),
  port: parseInt(requireFromEnv('PORT'), 10),
  version: '1.0.0',
};
