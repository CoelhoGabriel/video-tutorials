/* eslint-disable promise/no-nesting */
import express from 'express';
import mongoose from 'mongoose';

type QueriesType = {
  loadHomePage: () => Promise<number>;
};

function createHandlers({ queries }: { queries: QueriesType }): {
  home: (req: any, res: any, next: any) => any;
} {
  function home(req, res, next): any {
    return queries
      .loadHomePage()
      .then((videosWatched) =>
        res.render('home/templates/home', { videosWatched }),
      )
      .catch(next);
  }

  return {
    home,
  };
}

function createQueries(): QueriesType {
  async function loadHomePage(): Promise<number> {
    const conn = await mongoose
      .createConnection()
      .openUri('mongodb://0.0.0.0:27017/event-store');
    const collect = conn.collection('videos');
    const video = await collect.findOne();

    return video?.videosWatched ?? 0;
  }

  return {
    loadHomePage,
  };
}

function createHome(): {
  handlers: any;
  queries: any;
  router: any;
} {
  const queries = createQueries();
  const handlers = createHandlers({ queries });

  const router = express.Router();

  router.route('/').get(handlers.home);

  return { handlers, queries, router };
}

export default createHome;
