function mountRoutes(app, config): void {
  app.use('/', config.homeApp.router);
}

export default mountRoutes;
