import express from 'express';
import { join } from 'path';

import attachLocals from '@/app/express/attach-locals';
import lastResortErrorHandler from '@/app/express/last-resort-error-handler';
import primeRequestContext from '@/app/express/prime-request-context';

function mountMiddleware(app, env): void {
  app.use(lastResortErrorHandler);
  app.use(primeRequestContext);
  app.use(attachLocals);
  app.use(
    express.static(join(__dirname, '..', 'public'), { maxAge: 86400000 }),
  );
}

export default mountMiddleware;
