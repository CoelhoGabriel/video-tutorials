import express from 'express';
import { join } from 'path';

import mountMiddleware from '@/app/express/mount-middleware';
import mountRoutes from '@/app/express/mount-routes';

function createExpressApp({ config, env }): any {
  const app = express();

  // Configure PUG
  app.set('views', join(__dirname, '..'));
  app.set('view engine', 'pug');

  mountMiddleware(app, env);
  mountRoutes(app, config);

  return app;
}

export default createExpressApp;
