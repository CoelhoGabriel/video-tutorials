import { v4 } from 'uuid';

function primeRequestContext(req, res, next): void {
  req.context = {
    traceId: v4(),
  };
  next();
}

export default primeRequestContext;
