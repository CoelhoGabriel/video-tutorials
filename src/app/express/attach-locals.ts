function attachLocals(req, res, next): void {
  res.locals.context = req.context;
  next();
}

export default attachLocals;
