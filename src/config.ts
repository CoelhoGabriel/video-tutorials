import createHomeApp from '@/app/home';
import mongoose from 'mongoose';

export async function createConfig({ env }): Promise<{
  env: any;
  homeApp: {
    handlers: any;
    queries: any;
    router: any;
  };
}> {
  await mongoose
    .createConnection()
    .openUri('mongodb://0.0.0.0:27017/event-store');

  const homeApp = createHomeApp();

  return {
    homeApp,
    env,
  };
}
